import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InternalAppRoutingModule } from './internal-app-routing.module';
import { InternalAppComponent } from './internal-app/internal-app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    InternalAppComponent,
    MainPageComponent,
  ],
  imports: [
    CommonModule,
    InternalAppRoutingModule,
    SharedModule
  ]
})
export class InternalAppModule { }
