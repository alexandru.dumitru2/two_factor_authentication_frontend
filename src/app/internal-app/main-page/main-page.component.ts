import { ThisReceiver } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/core/api/user.service';
import { ServerError } from 'src/app/core/constants/ServerError';
import { Urls } from 'src/app/core/constants/Urls';
import ComputePayloadHelper from 'src/app/core/Helpers/ComputePayloadHelper';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  token = new FormControl('', [])
  title?: string;
  isToken = false;
  seconds?: number;
  intervalSubject: any;
  message = "";

  constructor(
    private router: Router,
    private userService: UserService,
    private ref: ChangeDetectorRef,
    private toastr: ToastrService
    ) {
    this.router.url.indexOf('new-token') > -1 ? this.title = "Login" : this.title = "Register";
   }

  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  loginRegister(event: string){
    if(event == "Login"){
      this.userService.newToken(ComputePayloadHelper.computeRegister(this.email.value)).subscribe((response: any) => {
        this.token.patchValue(response.token);
        var receivingDate = new Date();
        this.userService.tokenReceived(ComputePayloadHelper.computeTokenReceived(this.email.value, this.token.value, receivingDate)).subscribe((response1) => {
          this.message = "Token recieved successfully."
          this.toastr.success(this.message ,"Success");
          this.seconds = 30;
          this.clearInterval();
          this.computeSeconds();
        },(error) => {
          if(error.message == ServerError.USER_NOT_FOUND){
            this.message = "This user does not exist";
            this.toastr.error(this.message, "Error");
          }
        });
      });
    }
    else {
      this.userService.register(ComputePayloadHelper.computeRegister(this.email.value)).subscribe((response: any) => {
        this.message = "User created";
        this.toastr.success(this.message, "Success");
        this.router.navigate([`${Urls.APP}/${Urls.NEW_TOKEN}`]);
      },(error) => {
        if(error.message == ServerError.USER_EXIST){
          this.message = "This user already exist";
          this.toastr.error(this.message, "Error");
        }
      })
    }
  }

  verifyToken() {
    var verificationDate = new Date();
    this.userService.verifyToken(ComputePayloadHelper.computeVerifyToken(this.email.value, this.token.value, verificationDate)).subscribe((response) => {
      this.message = "Token is valid";
      this.toastr.success(this.message, "Success");
    },
    (error) => {
      this.message = "Token is invalid";
      this.toastr.error(this.message, "Error");
    })
  }

  computeSeconds(){
    this.intervalSubject = setInterval(() => {
      if(this.seconds! > 0){
        this.seconds!--;
      }
      else{
        this.verifyToken();
        clearInterval(this.intervalSubject);
      }
    },1000);
  }

  clearInterval(){
    clearInterval(this.intervalSubject);
  }
}
