import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Urls } from '../core/constants/Urls';
import { InternalAppComponent } from './internal-app/internal-app.component';
import { MainPageComponent } from './main-page/main-page.component';

const routes: Routes = [
  {
    path: '',
    component: InternalAppComponent,
    children: [
      {
        path: '',
        redirectTo: Urls.NEW_TOKEN,
        pathMatch: 'full'
      },
      {
        path: Urls.NEW_TOKEN,
        component: MainPageComponent
      },
      {
        path: Urls.REGISTER,
        component: MainPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InternalAppRoutingModule { }
