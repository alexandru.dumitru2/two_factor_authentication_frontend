import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Urls } from './core/constants/Urls';

const routes: Routes = [
  {
    path: '',
    redirectTo: Urls.APP,
    pathMatch: 'full'
  },
  {
    path: Urls.APP,
    loadChildren: () => import('./internal-app/internal-app.module').then(m => m.InternalAppModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
