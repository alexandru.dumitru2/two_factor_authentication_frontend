import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { debounceTime, interval, pipe, Subscription, tap } from 'rxjs';
import { UserService } from './core/api/user.service';
import ComputePayloadHelper from './core/Helpers/ComputePayloadHelper';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  email?: string;
  token?: string;
  seconds?: number;
  interval?: any;
  message?: string;
  intervalSubject: any;

  constructor(private userService: UserService, private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.userService.newToken(ComputePayloadHelper.computeRegister("a@a.com")).subscribe((response1: any) => {
      this.token = response1.token;
      var receivingDate = new Date();
      this.userService.tokenReceived(ComputePayloadHelper.computeTokenReceived("a@a.com", this.token!, receivingDate)).subscribe((response2) => {
        this.seconds = 30;
        this.computeSeconds();
      });
    });
  }

  computeSeconds(){
    this.intervalSubject = setInterval(() => {
      if(this.seconds! > 0){
        this.seconds!--;
      }
      else{
        this.verifyToken();
        clearInterval(this.intervalSubject);
      }
    },1000);
  }
  verifyToken() {
    var verificationDate = new Date();
    this.userService.verifyToken(ComputePayloadHelper.computeVerifyToken("a@a.com", this.token!, verificationDate)).subscribe((response) => {
      this.message = "Token is valid";
    },
    (error) => {
      this.message = "Token is invalid";
    })
  }
}
