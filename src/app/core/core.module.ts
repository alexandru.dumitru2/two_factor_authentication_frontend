import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ApiService } from './api/api.service';
import { UserService } from './api/user.service';
import { Interceptor } from './interceptors/interceptor';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],

  declarations: [],

  providers: [
    ApiService,
    UserService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  exports: [

  ]
})

export class CoreModule {
}
