export interface Register {
  email: string;
}

export interface RecieveToken {
  token: string;
  email: string;
  tokenReceivingDate: Date;
}

export interface VerifyToken {
  token: string;
  email: string;
  tokenVerificationDate: Date;
}
