import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ApiService {
  private readonly apiUrl = "https://localhost:7284/api/authentication";

  constructor(private http: HttpClient) { }

  get(path: string, params = {}, headers = {}) {
    return this.http.get(`${this.apiUrl}${path}`, { params, headers });
  }

  put(path: string, body = {}) {
    return this.http.put(`${this.apiUrl}${path}`, body);
  }

  post(path: string, body = {}, params = {}) {
    return this.http.post(`${this.apiUrl}${path}`, body, { params });
  }

  delete(path: string, body = {}) {
    return this.http.delete(`${this.apiUrl}${path}`, { body: body });
  }
}
