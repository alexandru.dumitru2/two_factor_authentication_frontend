import { Injectable } from '@angular/core';
import { RecieveToken, Register, VerifyToken } from '../Models/User';
import { ApiService } from './api.service';


@Injectable()
export class UserService {

  constructor(private apiService: ApiService) { }

  register(body: Register) {
    return this.apiService.post('/register', body);
  }

  newToken(body: Register) {
    return this.apiService.post('/new-token', body);
  }

  tokenReceived(body: RecieveToken) {
    return this.apiService.post('/token-received', body);
  }

  verifyToken(body: VerifyToken) {
    return this.apiService.post('/verify-token', body);
  }
}
