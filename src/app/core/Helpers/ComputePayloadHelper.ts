export default class ComputePayloadHelper {
  public static computeRegister(email: string){
    return {
      email: email
    }
  }

  public static computeTokenReceived(email: string, token: string, tokenReceivingDate: Date){
    return {
      email: email,
      token: token,
      tokenReceivingDate: tokenReceivingDate
    }
  }

  public static computeVerifyToken(email: string, token: string, tokenVerificationDate: Date){
    return {
      email: email,
      token: token,
      tokenVerificationDate: tokenVerificationDate
    }
  }
}
